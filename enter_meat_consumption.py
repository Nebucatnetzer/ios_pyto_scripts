import json
import sys
import get_meat_consumption as meat

filename = 'meat_consumption.json'

def write_data(data):
    try:
        input_dict = json.loads(sys.argv[1])
    except Exception as e:
        print(e)
        return
    data.update(input_dict)    
    with open(filename, 'w') as f:
        json.dump(data, f)
    
data = meat.get_data()   
write_data(data)