import json
import sys


filename = 'meat_consumption.json'

def get_data():
    try:
        with open(filename) as f:
            return json.load(f)
    except FileNotFoundError:
        open(filename, 'w')
        return json.loads("{}")
    

def calculate_sum(data):
    return round(sum(data.values()), 4)
    
 
if __name__ == '__main__':
    data = get_data()
    print(calculate_sum(data))
